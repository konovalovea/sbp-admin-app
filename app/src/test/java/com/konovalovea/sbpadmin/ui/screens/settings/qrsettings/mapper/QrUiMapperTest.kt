package com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.mapper

import android.content.Context
import com.konovalovea.sbpadmin.R
import com.konovalovea.sbpadmin.api.entity.Order
import com.konovalovea.sbpadmin.api.entity.QrApi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

class QrUiMapperTest {

    private val context = mock(Context::class.java)
    private val qrUiMapper: QrUiMapper = QrUiMapper(context)

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `test mapToUi method when order has id`() {
        // arrange
        val order = Order(id = "order_id", amount = 100.0, qr = QrApi("qr_id", "info", "details"))
        `when`(context.getString(R.string.qr_enabled)).thenReturn("Enabled")
        `when`(context.getString(R.string.qr_disabled)).thenReturn("Disabled")
        `when`(context.getString(R.string.qr_current_amount_not_set)).thenReturn("Amount Not Set")
        `when`(context.getString(R.string.qr_current_amount_value, 100.0)).thenReturn("$100")

        // act
        val result = qrUiMapper.mapToUi(order)

        // assert
        assertEquals("qr_id", result.id)
        assertEquals("$100", result.amount)
        assertEquals("info", result.additionalInfo)
        assertEquals("details", result.paymentDetails)
        assertEquals("Enabled", result.status.value)
        assertEquals(R.color.green, result.status.color)
    }

    @Test
    fun `test mapToUi method when order does not have id`() {
        // arrange
        val order = Order(amount = 0.0, qr = QrApi("qr_id", "info", "details"))
        `when`(context.getString(R.string.qr_enabled)).thenReturn("Enabled")
        `when`(context.getString(R.string.qr_disabled)).thenReturn("Disabled")
        `when`(context.getString(R.string.qr_current_amount_not_set)).thenReturn("Amount Not Set")
        `when`(context.getString(R.string.qr_current_amount_value, 0.0)).thenReturn("$100")

        // act
        val result = qrUiMapper.mapToUi(order)

        // assert
        assertEquals("qr_id", result.id)
        assertEquals("Amount Not Set", result.amount)
        assertEquals("info", result.additionalInfo)
        assertEquals("details", result.paymentDetails)
        assertEquals("Disabled", result.status.value)
        assertEquals(R.color.red, result.status.color)
    }

    @Test
    fun `test mapToUi method when additionalInfo is not set`() {
        // Given
        val qr = QrApi("123", paymentDetails = "Payment details")
        val order = Order(id = "123", amount = 100.0, qr = qr)
        `when`(context.getString(R.string.qr_enabled)).thenReturn("Enabled")
        `when`(context.getString(R.string.qr_disabled)).thenReturn("Disabled")
        `when`(context.getString(R.string.qr_current_amount_value, 100.0)).thenReturn("100.0")

        // When
        val result = qrUiMapper.mapToUi(order)

        // Then
        assertEquals("123", result.id)
        assertEquals("100.0", result.amount)
        assertEquals("Enabled", result.status.value)
        assertEquals(R.color.green, result.status.color)
        assertNull(result.additionalInfo)
        assertEquals("Payment details", result.paymentDetails)
    }

    @Test
    fun `test mapToUi method when paymentDetails is not set`() {
        // Given
        val qr = QrApi("123", additionalInfo = "Additional info")
        val order = Order(id = "123", amount = 100.0, qr = qr)
        `when`(context.getString(R.string.qr_enabled)).thenReturn("Enabled")
        `when`(context.getString(R.string.qr_disabled)).thenReturn("Disabled")
        `when`(context.getString(R.string.qr_current_amount_value, 100.0)).thenReturn("100.0")

        // When
        val result = qrUiMapper.mapToUi(order)

        // Then
        assertEquals("123", result.id)
        assertEquals("100.0", result.amount)
        assertEquals("Enabled", result.status.value)
        assertEquals(R.color.green, result.status.color)
        assertEquals("Additional info", result.additionalInfo)
        assertNull(result.paymentDetails)
    }
}