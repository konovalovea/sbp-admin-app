package com.konovalovea.sbpadmin

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import com.konovalovea.sbpadmin.ui.MainApp
import com.konovalovea.sbpadmin.ui.theme.SbpAdminTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SbpAdminTheme {
                MainApp()
            }
        }
    }
}
