package com.konovalovea.sbpadmin.ui.common

import android.widget.Toast
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext

@Composable
fun ShowMessage(message: String, onMessageShown: () -> Unit) {
    val context = LocalContext.current
    Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    onMessageShown()
}