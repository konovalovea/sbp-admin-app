package com.konovalovea.sbpadmin.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFFFEE600)
val Purple700 = Color(0xFF3700B3)

val Teal200 = Color(0xFF03DAC5)
val Green100 = Color(0xffd8e9b5)
val Green600 = Color(0xff73a411)
val Green700 = Color(0xff628a13)
val Purple100 = Color(0xffc6b5e9)
val Black = Color(0xff000000)
val DarkGrey = Color(0xff4d4d4d)
val DarkerGrey = Color(0xff292929)
val IndicatorBlue = Color(0xff187ede)