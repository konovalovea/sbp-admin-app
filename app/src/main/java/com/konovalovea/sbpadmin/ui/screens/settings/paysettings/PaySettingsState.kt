package com.konovalovea.sbpadmin.ui.screens.settings.paysettings

import androidx.annotation.StringRes
import com.konovalovea.sbpadmin.ui.screens.settings.paysettings.entity.Settings

sealed class PaySettingsState {

    object Loading : PaySettingsState()

    data class Loaded(
        val settings: Settings,
        @StringRes val messageId: Int? = null
    ): PaySettingsState()
}

