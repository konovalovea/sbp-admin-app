package com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.mapper

import android.content.Context
import com.konovalovea.sbpadmin.R
import com.konovalovea.sbpadmin.api.entity.Order
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity.QrUiItem
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class QrUiMapper @Inject constructor(
    @ApplicationContext private val context: Context
) {

    fun mapToUi(order: Order): QrUiItem {
        val qr = order.qr!!
        return QrUiItem(
            id = qr.id,
            amount = formatAmount(order.amount),
            additionalInfo = qr.additionalInfo,
            paymentDetails = qr.paymentDetails,
            status = formatStatus(order)
        )
    }

    private fun formatAmount(amount: Double?): String {
        return if (amount == null || amount == 0.0) {
            context.getString(R.string.qr_current_amount_not_set)
        } else {
            context.getString(R.string.qr_current_amount_value, amount)
        }
    }

    private fun formatStatus(order: Order): QrUiItem.Status {
        return order.id?.let {
            QrUiItem.Status(context.getString(R.string.qr_enabled), R.color.green)
        } ?: QrUiItem.Status(context.getString(R.string.qr_disabled), R.color.red)
    }
}