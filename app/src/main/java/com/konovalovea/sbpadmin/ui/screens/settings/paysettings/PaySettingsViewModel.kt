package com.konovalovea.sbpadmin.ui.screens.settings.paysettings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.konovalovea.sbpadmin.api.entity.FiscType
import com.konovalovea.sbpadmin.api.repository.PaySettingsRepository
import com.konovalovea.sbpadmin.ui.screens.settings.paysettings.entity.Settings
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class PaySettingsViewModel @Inject constructor(
    private val paySettingsRepository: PaySettingsRepository,
) : ViewModel() {

    private val _uiState: MutableStateFlow<PaySettingsState> =
        MutableStateFlow(PaySettingsState.Loading)
    val uiState: StateFlow<PaySettingsState> = _uiState

    init {
        viewModelScope.launch {
            try {
                val fiscType = paySettingsRepository.getFiscType()
                val state = when (fiscType) {
                    FiscType.NOFISC -> PaySettingsState.Loaded(
                        Settings(isFiscalizationEnabled = false, fiscalizationVersion = FiscType.FISC12)
                    )
                    else -> PaySettingsState.Loaded(
                        Settings(isFiscalizationEnabled = true, fiscalizationVersion = fiscType)
                    )
                }
                _uiState.value = state
            } catch (e: Exception) {
                Timber.e(e, "Error in PaySettingsViewModel init")
            }
        }
    }

    fun onFiscalizationEnabled(enabled: Boolean) {
        viewModelScope.launch {
            try {
                val state = _uiState.value as PaySettingsState.Loaded
                if (enabled) {
                    paySettingsRepository.setFiscType(fiscType = state.settings.fiscalizationVersion)
                } else {
                    paySettingsRepository.setFiscType(fiscType = FiscType.NOFISC)
                }
                _uiState.value = state.copy(
                    settings = state.settings.copy(isFiscalizationEnabled = enabled)
                )
            } catch (e: Exception) {
                Timber.e("Failed to update fiscalization state")
            }
        }
    }

    fun onFiscalizationVersionChanged(version: FiscType) {
        viewModelScope.launch {
            try {
                val state = _uiState.value as PaySettingsState.Loaded
                paySettingsRepository.setFiscType(fiscType = version)
                _uiState.value = state.copy(
                    settings = state.settings.copy(fiscalizationVersion = version)
                )
            } catch(e: Exception) {
                Timber.e("Failed to update fiscalization version")
            }
        }
    }

    fun onMessageShown() {
        val state = _uiState.value as PaySettingsState.Loaded
        _uiState.value = state.copy(
            messageId = null
        )
    }
}