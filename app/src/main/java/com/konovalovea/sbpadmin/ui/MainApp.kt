package com.konovalovea.sbpadmin.ui

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.konovalovea.sbpadmin.ui.screens.NavGraphs
import com.ramcosta.composedestinations.DestinationsNavHost

@Composable
fun MainApp(modifier: Modifier = Modifier) {
    Scaffold(
        modifier = modifier.fillMaxSize()
    ) {
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            color = MaterialTheme.colors.background
        ) {
            SetUpNavigation()
        }
    }
}

@Composable
fun SetUpNavigation() {
    DestinationsNavHost(navGraph = NavGraphs.root)
}