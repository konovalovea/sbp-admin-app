package com.konovalovea.sbpadmin.ui.common.util

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.core.content.ContextCompat.getSystemService
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

interface ClipboardHelper {

    fun saveToClipboard(label: String, value: String)
}

class ClipboardHelperImpl @Inject constructor(
    @ApplicationContext private val context: Context
) : ClipboardHelper {

    override fun saveToClipboard(label: String, value: String) {
        val clipboard = getSystemService(context, ClipboardManager::class.java) as ClipboardManager
        val clip = ClipData.newPlainText(label, value)
        clipboard.setPrimaryClip(clip)
    }
}