package com.konovalovea.sbpadmin.ui.screens.qrscan

import androidx.annotation.StringRes

sealed class QrScanState {

    class Initial(
        @StringRes val messageId: Int? = null
    ) : QrScanState()

    object ScanningQr : QrScanState()

    class ScannedQr(val qrId: String) : QrScanState()
}