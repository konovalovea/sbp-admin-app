package com.konovalovea.sbpadmin.ui.screens.qrscan

import android.net.Uri
import androidx.lifecycle.ViewModel
import com.journeyapps.barcodescanner.ScanIntentResult
import com.konovalovea.sbpadmin.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

@HiltViewModel
class QrScanViewModel @Inject constructor() : ViewModel() {

    private val _uiState: MutableStateFlow<QrScanState> = MutableStateFlow(QrScanState.Initial())
    val uiState: StateFlow<QrScanState> = _uiState

    fun onQrScanButtonClick() {
        // for test
//        _uiState.value = QrScanState.ScannedQr("id")
        _uiState.value = QrScanState.ScanningQr
    }

    fun onQrScanResult(scanResult: ScanIntentResult?) {
        scanResult?.contents?.let { rawQrUrl ->
            val qrUri = Uri.parse(rawQrUrl)
            if (qrUri.authority == QR_AUTHORITY) {
                qrUri.pathSegments.getOrNull(0)
            } else {
                null
            }
        }?.let { qrId ->
            _uiState.value = QrScanState.ScannedQr(qrId)
        } ?: run {
            _uiState.value = QrScanState.Initial(R.string.qr_scan_error_message)
        }
    }

    fun onNavigated() {
        _uiState.value = QrScanState.Initial()
    }

    fun messageShown() {
        _uiState.value = QrScanState.Initial()
    }

    private companion object {
        const val QR_AUTHORITY = "qr.nspk.ru"
    }
}