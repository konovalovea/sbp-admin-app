package com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity

class StatisticsUiItem(
    val totalAmount: String,
    val operationCount: String,
    val operations: List<OperationUiItem>
)