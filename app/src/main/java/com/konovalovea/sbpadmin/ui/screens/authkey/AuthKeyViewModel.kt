package com.konovalovea.sbpadmin.ui.screens.authkey

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.konovalovea.sbpadmin.R
import com.konovalovea.sbpadmin.api.repository.AuthRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class AuthKeyViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : ViewModel() {

    private val _uiState: MutableStateFlow<AuthKeyState> =
        MutableStateFlow(AuthKeyState.Loaded(isValidKey = false))
    val uiState: StateFlow<AuthKeyState> = _uiState

    fun validateKey(key: String) {
        viewModelScope.launch {
            try {
                _uiState.value = AuthKeyState.Loaded(isValidatingKey = true, isValidKey = false)
                val isValidKey = authRepository.setApiKey(key)
                if (isValidKey) {
                    _uiState.value = AuthKeyState.Loaded(isValidKey = true)
                } else {
                    _uiState.value = AuthKeyState.Loaded(
                        isValidKey = false,
                        errorMessageId = R.string.invalid_key
                    )
                }
            } catch (e: Exception) {
                _uiState.value = AuthKeyState.Loaded(
                    isValidKey = false,
                    errorMessageId = R.string.validation_error
                )
                Timber.e("Error when validating api key", e)
            }
        }
    }

    fun onNavigated() {
        _uiState.value = AuthKeyState.Loaded(isValidKey = false)
    }
}