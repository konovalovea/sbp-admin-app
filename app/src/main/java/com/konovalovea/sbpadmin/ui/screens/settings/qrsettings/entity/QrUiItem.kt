package com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity

import androidx.annotation.ColorRes

class QrUiItem(
    val id: String,
    val amount: String,
    val additionalInfo: String?,
    val paymentDetails: String?,
    val status: Status
) {

    class Status(
        val value: String,
        @ColorRes val color: Int
    )
}