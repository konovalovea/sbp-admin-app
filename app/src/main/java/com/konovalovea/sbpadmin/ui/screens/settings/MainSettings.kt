package com.konovalovea.sbpadmin.ui.screens.settings

import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.rememberNavController
import com.konovalovea.sbpadmin.ui.screens.NavGraphs
import com.konovalovea.sbpadmin.ui.screens.appCurrentDestinationAsState
import com.konovalovea.sbpadmin.ui.screens.destinations.PaySettingsScreenDestination
import com.konovalovea.sbpadmin.ui.screens.destinations.QrSettingsScreenDestination
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.QrSettingsScreen
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.QrSettingsViewModel
import com.konovalovea.sbpadmin.ui.screens.startAppDestination
import com.konovalovea.sbpadmin.ui.theme.Icons
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.manualcomposablecalls.composable
import com.ramcosta.composedestinations.spec.Direction
import com.konovalovea.sbpadmin.ui.screens.destinations.Destination as DestinationClass

@Destination
@Composable
fun MainSettings(
    qrId: String,
    modifier: Modifier = Modifier,
) {
    val navController = rememberNavController()

    Scaffold(
        bottomBar = { BottomBar(navController) },
        modifier = modifier
    ) { innerPadding ->
        DestinationsNavHost(
            navGraph = NavGraphs.settings,
            modifier = Modifier.padding(innerPadding),
            navController = navController
        ) {
            composable(QrSettingsScreenDestination) {
                val viewModel = hiltViewModel<QrSettingsViewModel>().apply {
                    setQr(qrId = qrId)
                }
                QrSettingsScreen(viewModel = viewModel)
            }
        }
    }
}

@Composable
fun BottomBar(
    navController: NavController
) {
    val bottomBarItems = listOf(
        BottomBarItem.QrSettings(),
        BottomBarItem.PaySettings()
    )

    val currentDestination: DestinationClass = navController.appCurrentDestinationAsState().value
        ?: NavGraphs.root.startAppDestination
    val showBottomBar = bottomBarItems
        .any { bottomBarItem -> bottomBarItem.destination == currentDestination }

    if (showBottomBar) {
        BottomNavigation(navController, bottomBarItems, currentDestination)
    }
}

@Composable
fun BottomNavigation(
    navController: NavController,
    bottomBarItems: List<BottomBarItem>,
    currentDestination: DestinationClass
) {
    BottomNavigation(
        backgroundColor = Color.White,
        elevation = 16.dp
    ) {
        bottomBarItems.forEach { item ->
            BottomNavigationItem(
                selected = currentDestination == item.destination,
                onClick = { navigateWithController(navController, item) },
                icon = item.icon,
                selectedContentColor = LocalContentColor.current,
                unselectedContentColor = LocalContentColor.current.copy(alpha = ContentAlpha.disabled)
            )
        }
    }
}

fun navigateWithController(navController: NavController, item: BottomBarItem) {
    navController.navigate(item.destination.route) {
        popUpTo(navController.graph.findStartDestination().id) {
            saveState = true
        }
        launchSingleTop = true
        restoreState = true
    }
}

sealed class BottomBarItem(
    val destination: Direction,
    val icon: @Composable () -> Unit
) {

    class QrSettings : BottomBarItem(
        destination = QrSettingsScreenDestination,
        icon = { Icon(Icons.QrCode, contentDescription = null) }
    )

    class PaySettings : BottomBarItem(
        destination = PaySettingsScreenDestination,
        icon = { Icon(Icons.Settings, contentDescription = null) }
    )
}
