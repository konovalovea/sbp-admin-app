package com.konovalovea.sbpadmin.ui.screens.authkey

import androidx.annotation.StringRes

sealed class AuthKeyState {

    data class Loaded(
        val isValidKey: Boolean,
        val isValidatingKey: Boolean = false,
        @StringRes val errorMessageId: Int? = null
    ) : AuthKeyState()
}