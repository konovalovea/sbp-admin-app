package com.konovalovea.sbpadmin.ui.screens.authkey

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.konovalovea.sbpadmin.R
import com.konovalovea.sbpadmin.ui.screens.destinations.QrScanScreenDestination
import com.konovalovea.sbpadmin.ui.theme.SbpAdminTheme
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@Destination
@Composable
fun AuthKeyScreen(
    modifier: Modifier = Modifier,
    viewModel: AuthKeyViewModel = hiltViewModel(),
    navigator: DestinationsNavigator,
) {
    val uiState by viewModel.uiState.collectAsState()

    AuthKeyScreenContent(
        modifier = modifier,
        uiState = uiState,
        onSendButtonClick = { key -> viewModel.validateKey(key) },
        onValidKey = {
            navigator.popBackStack()
            navigator.navigate(QrScanScreenDestination())
        }
    )
}

@Composable
fun AuthKeyScreenContent(
    modifier: Modifier = Modifier,
    uiState: AuthKeyState,
    onSendButtonClick: (String) -> Unit,
    onValidKey: () -> Unit,
) {
    when (uiState) {
        is AuthKeyState.Loaded -> {
            if (uiState.isValidKey) {
                onValidKey()
            } else {
                AuthKeyScreenLoaded(modifier, uiState, onSendButtonClick)
            }
        }
    }
}

@Composable
fun AuthKeyScreenLoaded(
    modifier: Modifier = Modifier,
    uiState: AuthKeyState.Loaded,
    onSendButtonClick: (String) -> Unit,
) {
    var key by remember { mutableStateOf("") }

    Column(
        verticalArrangement = Arrangement.spacedBy(16.dp),
        modifier = modifier
            .padding(vertical = 16.dp)
    ) {
        Text(
            text = stringResource(R.string.auth_key_prompt),
            style = MaterialTheme.typography.h5,
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .padding(bottom = 8.dp)
        )
        TextField(
            value = key,
            label = { Text(stringResource(id = R.string.key)) },
            onValueChange = { value -> key = value },
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(horizontal = 16.dp)
        )
        if (uiState.errorMessageId != null) {
            Text(
                text = stringResource(uiState.errorMessageId),
                color = Color.Red,
                modifier = Modifier.padding(horizontal = 16.dp)
            )
        }
        Button(
            onClick = { onSendButtonClick(key) },
            elevation = null,
            enabled = !uiState.isValidatingKey && key.isNotBlank(),
            modifier = Modifier
                .wrapContentHeight()
                .align(Alignment.End)
                .padding(horizontal = 16.dp)
        ) {
            Text(stringResource(R.string.auth_key_send))
        }
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun AuthKeyScreenLoadedPreview() {
    SbpAdminTheme {
        AuthKeyScreenLoaded(
            uiState = AuthKeyState.Loaded(
                isValidKey = false,
                isValidatingKey = true,
                errorMessageId = null
            ),
            onSendButtonClick = { },
        )
    }
}