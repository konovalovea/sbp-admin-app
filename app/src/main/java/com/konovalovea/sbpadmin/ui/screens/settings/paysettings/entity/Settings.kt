package com.konovalovea.sbpadmin.ui.screens.settings.paysettings.entity

import com.konovalovea.sbpadmin.api.entity.FiscType

data class Settings(
    val isFiscalizationEnabled: Boolean,
    val fiscalizationVersion: FiscType
)