package com.konovalovea.sbpadmin.ui.screens.settings.qrsettings

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.konovalovea.sbpadmin.R
import com.konovalovea.sbpadmin.api.entity.Order
import com.konovalovea.sbpadmin.api.entity.QrApi
import com.konovalovea.sbpadmin.api.repository.QrRepository
import com.konovalovea.sbpadmin.ui.common.util.ClipboardHelper
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.mapper.QrUiMapper
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.mapper.StatisticsUiMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class QrSettingsViewModel @Inject constructor(
    private val qrRepository: QrRepository,
    private val clipboardHelper: ClipboardHelper,
    private val qrUiMapper: QrUiMapper,
    private val statisticsUiMapper: StatisticsUiMapper,
    private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    private val _uiState: MutableStateFlow<QrSettingsState> =
        MutableStateFlow(QrSettingsState.Loading)
    val uiState: StateFlow<QrSettingsState> = _uiState

    private lateinit var qrId: String

    fun setQr(qrId: String) {
        if (!this::qrId.isInitialized) {
            this.qrId = qrId
            loadQr(qrId)
        }
    }

    private fun loadQr(qrId: String) {
        viewModelScope.launch {
            try {
                _uiState.value = QrSettingsState.Loading
                val qrUiItem = viewModelScope.async {
                    val order = qrRepository.getOrder(qrId = qrId)
                    qrUiMapper.mapToUi(order)
                }
                val statisticsUiItem = viewModelScope.async {
                    val statistics = qrRepository.getStatistics(qrId = qrId)
                    statisticsUiMapper.mapToUi(statistics)
                }
                _uiState.value = QrSettingsState.QrLoaded(
                    qrUiItem = qrUiItem.await(),
                    statistics = statisticsUiItem.await()
                )
            } catch (e: Exception) {
                _uiState.value = QrSettingsState.Error
                Timber.e(e, "Error in loadQr")
            }
        }
    }

    fun reloadQr() {
        loadQr(qrId)
    }

    fun updateQrAmount(amount: String) {
        viewModelScope.launch {
            try {
                val order = qrRepository.createOrder(
                    Order(amount = amount.toDouble(), qr = QrApi(id = qrId))
                )
                val qrUiItem = qrUiMapper.mapToUi(order)
                val state = _uiState.value
                if (state is QrSettingsState.QrLoaded) {
                    _uiState.value = state.copy(qrUiItem = qrUiItem)
                }
                showMessage(R.string.amount_set)
            } catch (e: Exception) {
                showMessage(R.string.unable_to_set_amount)
                Timber.e(e, "Error in updateQrAmount")
            }
        }
    }

    fun copyToClipboard(value: String) {
        clipboardHelper.saveToClipboard(CLIPBOARD_QR_LABEL, value)
        showMessage(R.string.copied_to_clipboard)
    }

    fun updateStatistics() {
        viewModelScope.launch {
            val state = _uiState.value
            if (state !is QrSettingsState.QrLoaded) return@launch
            try {
                _uiState.value = state.copy(refreshing = true)
                val qrUiItem = viewModelScope.async {
                    val order = qrRepository.getOrder(qrId = qrId)
                    qrUiMapper.mapToUi(order)
                }
                val statisticsUiItem = viewModelScope.async {
                    val statistics = qrRepository.getStatistics(qrId = qrId)
                    statisticsUiMapper.mapToUi(statistics)
                }
                _uiState.value = state.copy(
                    qrUiItem = qrUiItem.await(),
                    statistics = statisticsUiItem.await(),
                    refreshing = false
                )
            } catch (e: Exception) {
                showMessage(R.string.qr_unable_to_refresh)
                _uiState.value = state.copy(
                    refreshing = false
                )
                Timber.e(e, "Error in onRefresh")
            }
        }
    }

    fun clearStatistics() {
        viewModelScope.launch {
            try {
                val state = _uiState.value
                if (state is QrSettingsState.QrLoaded) {
                    qrRepository.clearStatistics(qrId = qrId)
                    val statistics = qrRepository.getStatistics(qrId = qrId)
                    val statisticsUiItem = statisticsUiMapper.mapToUi(statistics)
                    _uiState.value = state.copy(
                        statistics = statisticsUiItem
                    )
                }
            } catch (e: Exception) {
                showMessage(R.string.qr_unable_to_clear_stats)
                Timber.e(e, "Error in clearStatistics")
            }
        }
    }

    private fun showMessage(messageId: Int?) {
        val state = _uiState.value
        if (state is QrSettingsState.QrLoaded) {
            _uiState.value = state.copy(messageId = messageId)
        }
    }

    fun messageShown() {
        showMessage(null)
    }

    private companion object {
        const val CLIPBOARD_QR_LABEL = "QR value"
    }
}