package com.konovalovea.sbpadmin.ui.screens.qrscan

import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.journeyapps.barcodescanner.ScanContract
import com.journeyapps.barcodescanner.ScanOptions
import com.konovalovea.sbpadmin.R
import com.konovalovea.sbpadmin.ui.common.ShowMessage
import com.konovalovea.sbpadmin.ui.screens.destinations.MainSettingsDestination
import com.konovalovea.sbpadmin.ui.theme.SbpAdminTheme
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator

@RootNavGraph(start = true)
@Destination
@Composable
fun QrScanScreen(
    modifier: Modifier = Modifier,
    viewModel: QrScanViewModel = hiltViewModel(),
    navigator: DestinationsNavigator,
) {
    val scanLauncher = rememberLauncherForActivityResult(
        contract = ScanContract(),
        onResult = { result -> viewModel.onQrScanResult(result) }
    )

    val uiState by viewModel.uiState.collectAsState()
    when (val state = uiState) {
        is QrScanState.Initial -> {
            QrScanScreenContent(
                onButtonClick = { viewModel.onQrScanButtonClick() },
                modifier = modifier
            )
            if (state.messageId != null) {
                ShowMessage(message = stringResource(state.messageId)) { viewModel.messageShown() }
            }
        }
        is QrScanState.ScanningQr -> {
            LaunchScanningScreen(scanLauncher)
        }
        is QrScanState.ScannedQr -> {
            navigateToMainSettings(navigator, state.qrId, viewModel)
        }
    }
}

@Composable
fun QrScanScreenContent(
    onButtonClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = Modifier.fillMaxWidth().fillMaxHeight(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = stringResource(R.string.scan_qr_text),
            style = MaterialTheme.typography.body1
        )
        Spacer(Modifier.height(16.dp))
        Button(
            onClick = onButtonClick,
            elevation = null,
            modifier = Modifier.wrapContentSize()
        ) {
            Text(stringResource(R.string.scan_qr))
        }
    }
}

@Composable
fun LaunchScanningScreen(scanLauncher: ActivityResultLauncher<ScanOptions>) {
    val scanOptions = ScanOptions().apply {
        setDesiredBarcodeFormats(ScanOptions.QR_CODE)
        setBeepEnabled(false)
        setPrompt(stringResource(R.string.scan_qr_prompt))
    }
    scanLauncher.launch(scanOptions)
}

fun navigateToMainSettings(
    navigator: DestinationsNavigator,
    qrId: String,
    viewModel: QrScanViewModel
) {
    navigator.navigate(MainSettingsDestination(qrId = qrId))
    viewModel.onNavigated()
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun QrScanScreenPreview() {
    SbpAdminTheme {
        QrScanScreenContent({ })
    }
}