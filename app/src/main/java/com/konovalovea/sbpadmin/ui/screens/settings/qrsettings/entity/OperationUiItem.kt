package com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity

import kotlinx.serialization.SerialName

class OperationUiItem(
    @SerialName("orderId")
    val id: String,
    @SerialName("buyDate")
    val date: String,
    @SerialName("amount")
    val amount: String = "vald ne sdelal",
    @SerialName("isDone")
    val status: String
)