package com.konovalovea.sbpadmin.ui.screens.settings.qrsettings

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Delete
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.konovalovea.sbpadmin.R
import com.konovalovea.sbpadmin.ui.common.ShowMessage
import com.konovalovea.sbpadmin.ui.screens.settings.SettingsNavGraph
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity.OperationUiItem
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity.QrUiItem
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity.StatisticsUiItem
import com.konovalovea.sbpadmin.ui.theme.IndicatorBlue
import com.konovalovea.sbpadmin.ui.theme.SbpAdminTheme
import com.ramcosta.composedestinations.annotation.Destination

@SettingsNavGraph(start = true)
@Destination
@Composable
fun QrSettingsScreen(
    modifier: Modifier = Modifier,
    viewModel: QrSettingsViewModel,
) {
    val uiState by viewModel.uiState.collectAsState()

    QrSettingsScreenContent(
        uiState = uiState,
        onButtonClick = { amount -> viewModel.updateQrAmount(amount) },
        onQrPropertyClick = { value -> viewModel.copyToClipboard(value) },
        onMessageShown = { viewModel.messageShown() },
        onReloadButtonClick = { viewModel.reloadQr() },
        onRefresh = { viewModel.updateStatistics() },
        onStatisticsClearButtonClick = { viewModel.clearStatistics() },
        modifier = modifier
    )
}

@Composable
fun QrSettingsScreenContent(
    uiState: QrSettingsState,
    onButtonClick: (String) -> Unit,
    onQrPropertyClick: (String) -> Unit,
    onMessageShown: () -> Unit,
    onReloadButtonClick: () -> Unit,
    onRefresh: () -> Unit,
    onStatisticsClearButtonClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    when (uiState) {
        is QrSettingsState.Loading -> QrSettingsScreenLoading()
        is QrSettingsState.Error -> QrSettingsScreenError(onReloadButtonClick)
        is QrSettingsState.QrLoaded -> {
            QrSettingsScreenLoaded(
                uiState,
                onButtonClick = onButtonClick,
                onQrPropertyClick = onQrPropertyClick,
                onMessageShown = onMessageShown,
                onRefresh = onRefresh,
                onStatisticsClearButtonClick = onStatisticsClearButtonClick,
                modifier = modifier
            )
        }
    }
}

@Composable
fun QrSettingsScreenLoading() {
    Box(modifier = Modifier.fillMaxSize()) {
        CircularProgressIndicator(
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
fun QrSettingsScreenError(onReloadButtonClick: () -> Unit) {
    Box(modifier = Modifier.fillMaxSize()) {
        Column(modifier = Modifier.align(Alignment.Center)) {
            Text(stringResource(R.string.qr_error_message))
            Spacer(modifier = Modifier.height(8.dp))
            Button(elevation = null, onClick = onReloadButtonClick) {
                Text(stringResource(R.string.qr_retry_load))
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun QrSettingsScreenLoaded(
    uiState: QrSettingsState.QrLoaded,
    onButtonClick: (String) -> Unit,
    onQrPropertyClick: (String) -> Unit,
    onMessageShown: () -> Unit,
    onRefresh: () -> Unit,
    onStatisticsClearButtonClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    var amount by remember { mutableStateOf("") }
    var isErrorInput by remember { mutableStateOf(false) }
    val isEmptyInput = amount.isBlank()
    val refreshState = rememberPullRefreshState(uiState.refreshing, onRefresh)
    val isClearDialogOpen = remember { mutableStateOf(false) }

    if (isClearDialogOpen.value) {
        AlertDialog(
            onDismissRequest = { isClearDialogOpen.value = false },
            title = { Text("Удаление статистики") },
            text = { Text("Удалить статистику транзакций по QR?") },
            confirmButton = {
                Button(
                    onClick = {
                        isClearDialogOpen.value = false
                        onStatisticsClearButtonClick()
                    },
                    elevation = null
                ) {
                    Text("Да")
                }
            },
            dismissButton = {
                Button(
                    onClick = { isClearDialogOpen.value = false },
                    elevation = null
                ) {
                    Text("Отменить")
                }
            }
        )
    }

    Box(modifier = Modifier.pullRefresh(refreshState)) {
        LazyColumn {
            item {
                Spacer(Modifier.height(16.dp))
                Text(
                    text = stringResource(R.string.cash_qr),
                    style = MaterialTheme.typography.h4,
                    modifier = Modifier.padding(horizontal = 16.dp)
                )
                Spacer(Modifier.height(16.dp))
                Text(
                    text = stringResource(R.string.qr_info),
                    style = MaterialTheme.typography.h5,
                    modifier = Modifier.padding(horizontal = 16.dp)
                )
                Spacer(Modifier.height(8.dp))
                QrItem(
                    qrUiItem = uiState.qrUiItem,
                    onQrPropertyClick = onQrPropertyClick
                )
                Spacer(Modifier.height(16.dp))
                TextField(
                    value = amount,
                    label = { Text(stringResource(id = R.string.amount)) },
                    onValueChange = { rawValue ->
                        val value = rawValue.toDoubleOrNull()
                        if (value == null || value < 1) {
                            amount = rawValue
                            isErrorInput = true
                        } else if (value <= 1000000) {
                            amount = rawValue
                            isErrorInput = false
                        }
                    },
                    isError = isErrorInput,
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                    colors = TextFieldDefaults.textFieldColors(
                        cursorColor = IndicatorBlue,
                        focusedIndicatorColor = IndicatorBlue,
                        focusedLabelColor = IndicatorBlue
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(horizontal = 16.dp)
                )
                Spacer(Modifier.height(16.dp))
                Button(
                    onClick = { onButtonClick(amount) },
                    elevation = null,
                    enabled = !isErrorInput && !isEmptyInput,
                    modifier = Modifier
                        .wrapContentHeight()
                        .align(Alignment.CenterEnd)
                        .padding(horizontal = 16.dp)
                ) {
                    Text(stringResource(R.string.submit_amount))
                }
                Spacer(Modifier.height(12.dp))
            }

            item {
                Row(
                    modifier = Modifier.padding(horizontal = 16.dp)
                ) {
                    Text(
                        text = stringResource(R.string.qr_statistics),
                        style = MaterialTheme.typography.h5,
                    )
                    Spacer(Modifier.weight(1f))
                    Icon(
                        imageVector = Icons.Outlined.Delete,
                        contentDescription = null,
                        modifier = Modifier
                            .align(Alignment.CenterVertically)
                            .clickable(
                                indication = null,
                                interactionSource = remember { MutableInteractionSource() }
                            ) { isClearDialogOpen.value = true }
                    )
                }
                Spacer(Modifier.height(8.dp))
                Column(
                    modifier = modifier
                ) {
                    ItemPropertyLine(
                        name = stringResource(R.string.qr_total_sum),
                        value = uiState.statistics.totalAmount,
                    )
                    ItemPropertyLine(
                        name = stringResource(R.string.qr_order_count),
                        value = uiState.statistics.operationCount,
                    )
                }
                Spacer(Modifier.height(16.dp))
                Text(
                    text = stringResource(R.string.qr_orders),
                    style = MaterialTheme.typography.body1,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier.padding(horizontal = 16.dp)
                )
                Spacer(Modifier.height(10.dp))
            }

            if (uiState.statistics.operations.isEmpty()) {
                item {
                    Text(
                        text = stringResource(R.string.qr_no_orders),
                        textAlign = TextAlign.Center,
                        color = Color.Gray,
                        style = MaterialTheme.typography.body1,
                        modifier = Modifier
                            .padding(horizontal = 16.dp, vertical = 32.dp)
                            .fillMaxWidth()
                    )
                }
            }
            uiState.statistics.operations.forEachIndexed { position, operation ->
                item {
                    if (position != 0) Divider()
                    Column(
                        modifier = Modifier.padding(vertical = 2.dp)
                    ) {
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(vertical = 4.dp, horizontal = 16.dp)
                        ) {
                            Text(
                                text = operation.id,
                                modifier = Modifier
                                    .wrapContentSize()
                                    .padding(end = 16.dp)
                            )
                            Text(
                                text = operation.amount,
                                textAlign = TextAlign.End,
                                modifier = Modifier.fillMaxWidth(),
                                maxLines = 1,
                                overflow = TextOverflow.Ellipsis
                            )
                        }
                        Spacer(modifier = Modifier.height(4.dp))
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(vertical = 4.dp, horizontal = 16.dp)
                        ) {
                            Text(
                                text = operation.status,
                                modifier = Modifier
                                    .wrapContentSize()
                                    .padding(end = 16.dp)
                            )
                            Text(
                                text = operation.date,
                                textAlign = TextAlign.End,
                                modifier = Modifier.fillMaxWidth(),
                                maxLines = 1,
                                overflow = TextOverflow.Ellipsis
                            )
                        }
                    }
                }
            }

            item {
                Spacer(Modifier.height(16.dp))
            }
        }
        PullRefreshIndicator(uiState.refreshing, refreshState, Modifier.align(Alignment.TopCenter))
    }

    if (uiState.messageId != null) {
        ShowMessage(stringResource(uiState.messageId), onMessageShown)
    }
}

@Composable
fun QrItem(
    qrUiItem: QrUiItem,
    onQrPropertyClick: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    Column(
        modifier = modifier
    ) {
        ItemPropertyLine(
            name = stringResource(R.string.qr_status),
            value = qrUiItem.status.value,
            valueColor = colorResource(qrUiItem.status.color)
        )
        ItemPropertyLine(
            name = stringResource(R.string.qr_id),
            value = qrUiItem.id,
            onPropertyClick = onQrPropertyClick
        )
        ItemPropertyLine(
            name = stringResource(R.string.qr_current_amount),
            value = qrUiItem.amount,
            onPropertyClick = onQrPropertyClick
        )
        qrUiItem.additionalInfo?.let {
            ItemPropertyLine(
                name = stringResource(R.string.qr_additional_info),
                value = it,
                onPropertyClick = onQrPropertyClick
            )
        }
        qrUiItem.paymentDetails?.let {
            ItemPropertyLine(
                name = stringResource(R.string.qr_payment_details),
                value = it,
                onPropertyClick = onQrPropertyClick
            )
        }
    }
}

@Composable
fun ItemPropertyLine(
    name: String,
    value: String,
    onPropertyClick: ((String) -> Unit)? = null,
    valueColor: Color = Color.Unspecified
) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .run { if (onPropertyClick != null) clickable { onPropertyClick(value) } else this }
        .padding(vertical = 6.dp, horizontal = 16.dp)
    ) {
        Text(
            text = name,
            fontWeight = FontWeight.Bold,
            modifier = Modifier
                .wrapContentSize()
                .padding(end = 16.dp)
        )
        Text(
            text = value,
            color = valueColor,
            textAlign = TextAlign.End,
            modifier = Modifier.fillMaxWidth(),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun QrSettingsScreenLoadedPreview() {
    SbpAdminTheme {
        QrSettingsScreenLoaded(
            uiState = QrSettingsState.QrLoaded(
                qrUiItem = QrUiItem(
                    id = "ASF5C763A1A7483994C49FB0178F78D3",
                    amount = "100",
                    additionalInfo = "Дополнительная информация",
                    paymentDetails = "Детали платежа",
                    status = QrUiItem.Status(stringResource(R.string.qr_disabled), R.color.red)
                ),
                statistics = StatisticsUiItem(
                    totalAmount = "100",
                    operationCount = "10",
                    operations = listOf(
                        OperationUiItem(
                            id = "id",
                            date = "date",
                            amount = "amount",
                            status = "status"
                        ),
                        OperationUiItem(
                            id = "id",
                            date = "date",
                            amount = "amount",
                            status = "status"
                        ),
                        OperationUiItem(
                            id = "id",
                            date = "date",
                            amount = "amount",
                            status = "status"
                        )
                    )
                ),
                refreshing = true
            ),
            { },
            { },
            { },
            { },
            { }
        )
    }
}