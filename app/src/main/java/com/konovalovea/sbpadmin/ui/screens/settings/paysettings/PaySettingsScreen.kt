@file:OptIn(ExperimentalMaterialApi::class)

package com.konovalovea.sbpadmin.ui.screens.settings.paysettings

import androidx.compose.animation.*
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.konovalovea.sbpadmin.R
import com.konovalovea.sbpadmin.api.entity.FiscType
import com.konovalovea.sbpadmin.ui.common.ShowMessage
import com.konovalovea.sbpadmin.ui.screens.settings.SettingsNavGraph
import com.konovalovea.sbpadmin.ui.screens.settings.paysettings.entity.Settings
import com.konovalovea.sbpadmin.ui.theme.SbpAdminTheme
import com.ramcosta.composedestinations.annotation.Destination

const val DISABLED_UI_ALPHA = 0.3f

@SettingsNavGraph
@Destination
@Composable
fun PaySettingsScreen(
    modifier: Modifier = Modifier,
    viewModel: PaySettingsViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsState()

    PaySettingsScreenContent(
        modifier = modifier,
        onFiscalChecked = viewModel::onFiscalizationEnabled,
        onVersionDropDownClick = viewModel::onFiscalizationVersionChanged,
        onMessageShown = viewModel::onMessageShown,
        uiState = uiState
    )
}

@Composable
fun PaySettingsScreenContent(
    modifier: Modifier = Modifier,
    onFiscalChecked: (Boolean) -> Unit,
    onVersionDropDownClick: (FiscType) -> Unit,
    onMessageShown: () -> Unit,
    uiState: PaySettingsState,
) {
    when (uiState) {
        is PaySettingsState.Loaded -> {
            PaySettingsScreenLoaded(
                modifier = modifier,
                onFiscalChecked = onFiscalChecked,
                onVersionDropDownClick = onVersionDropDownClick,
                uiState = uiState
            )
            if (uiState.messageId != null) {
                ShowMessage(stringResource(uiState.messageId), onMessageShown)
            }
        }
        is PaySettingsState.Loading -> {
            PaySettingsScreenLoading()
        }
    }
}

@Composable
fun PaySettingsScreenLoading() {
    Box(modifier = Modifier.fillMaxSize()) {
        CircularProgressIndicator(
            modifier = Modifier.align(Alignment.Center)
        )
    }
}

@Composable
fun PaySettingsScreenLoaded(
    modifier: Modifier = Modifier,
    onFiscalChecked: (Boolean) -> Unit,
    onVersionDropDownClick: (FiscType) -> Unit,
    uiState: PaySettingsState.Loaded,
) {
    val dropDownExpanded = remember { mutableStateOf(false) }

    Column(
        modifier = modifier
            .padding(vertical = 16.dp)
    ) {
        Text(
            text = stringResource(R.string.pay_settings_title),
            style = MaterialTheme.typography.h4,
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .padding(bottom = 8.dp)
        )
        Box(
            modifier = Modifier
                .clickable { onFiscalChecked(!uiState.settings.isFiscalizationEnabled) }
                .padding(horizontal = 16.dp)
                .height(48.dp)
                .fillMaxWidth()
        ) {
            Text(
                text = "Фискализация",
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .wrapContentSize()
                    .padding(end = 16.dp)
            )
            CompositionLocalProvider(LocalMinimumTouchTargetEnforcement provides false) {
                Checkbox(
                    checked = uiState.settings.isFiscalizationEnabled,
                    onCheckedChange = onFiscalChecked,
                    modifier = Modifier
                        .align(Alignment.CenterEnd)
                        .height(8.dp)
                )
            }
        }

        AnimatedVisibility(
            visible = uiState.settings.isFiscalizationEnabled && uiState.settings.fiscalizationVersion != null,
            enter = slideInVertically() + fadeIn(),
            exit = slideOutVertically() + fadeOut()
        ) {
            Box(
                modifier = Modifier
                    .clickable { dropDownExpanded.value = !dropDownExpanded.value }
                    .padding(horizontal = 16.dp)
                    .height(48.dp)
                    .fillMaxWidth()
//                .run { if (!uiState.settings.isFiscalizationEnabled) alpha(DISABLED_UI_ALPHA) else this }
            ) {
                Text(
                    text = "Версия фискализации",
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .align(Alignment.CenterStart)
                        .wrapContentSize()
                        .padding(end = 16.dp)
                )
                Box(
                    modifier = Modifier
                        .align(Alignment.CenterEnd),
                ) {
                    Text(
                        text = when (uiState.settings.fiscalizationVersion) {
                            FiscType.FISC105 -> "1.05"
                            FiscType.FISC12 -> "1.20"
                            else -> ""
                        },
                        textAlign = TextAlign.End,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                    DropdownMenu(
                        expanded = dropDownExpanded.value,
                        modifier = Modifier.align(Alignment.CenterEnd),
                        onDismissRequest = { dropDownExpanded.value = false },
                    ) {
                        DropdownMenuItem(onClick = {
                            dropDownExpanded.value = false
                            onVersionDropDownClick(FiscType.FISC105)
                        }) { Text("1.05") }
                        DropdownMenuItem(onClick = {
                            dropDownExpanded.value = false
                            onVersionDropDownClick(FiscType.FISC12)
                        }) { Text("1.20") }
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun PaySettingsScreenLoadedPreview() {
    SbpAdminTheme {
        PaySettingsScreenLoaded(
            onFiscalChecked = { },
            onVersionDropDownClick = { },
            uiState = PaySettingsState.Loaded(
                settings = Settings(
                    isFiscalizationEnabled = true,
                    fiscalizationVersion = FiscType.FISC12
                )
            )
        )
    }
}