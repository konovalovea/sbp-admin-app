package com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.mapper

import android.content.Context
import android.icu.text.SimpleDateFormat
import com.konovalovea.sbpadmin.R
import com.konovalovea.sbpadmin.api.entity.Operation
import com.konovalovea.sbpadmin.api.entity.Statistics
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity.OperationUiItem
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity.StatisticsUiItem
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class StatisticsUiMapper @Inject constructor(
    @ApplicationContext private val context: Context
) {

    fun mapToUi(statistics: Statistics): StatisticsUiItem {
        return StatisticsUiItem(
            totalAmount = context.getString(R.string.statistics_amount, statistics.totalSum ?: 0.0),
            operationCount = (statistics.orderCount ?: 0).toString(),
            operations = statistics.operations
                ?.asReversed()
                ?.map { mapOperationToUi(it) } ?: emptyList()
        )
    }

    private fun mapOperationToUi(operation: Operation): OperationUiItem {
        return OperationUiItem(
            id = formatId(operation.id),
            date = formatDate(operation.date),
            amount = context.getString(R.string.statistics_amount, operation.amount),
            status = formatStatus(operation.isDone)
        )
    }

    private fun formatId(id: String): String {
        return "..." + id.takeLast(8)
    }

    private fun formatDate(isoDate: String): String {
        val date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").parse(isoDate)
        val targetFormat = SimpleDateFormat("dd.MM.YYYY HH:mm")
        return targetFormat.format(date)
    }

    private fun formatStatus(status: Boolean): String {
        return if (status) "Выполнена" else "Не выполнена"
    }
}