package com.konovalovea.sbpadmin.ui.screens.settings.qrsettings

import androidx.annotation.StringRes
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity.QrUiItem
import com.konovalovea.sbpadmin.ui.screens.settings.qrsettings.entity.StatisticsUiItem

sealed class QrSettingsState {

    object Loading : QrSettingsState()

    data class QrLoaded(
        val qrUiItem: QrUiItem,
        val statistics: StatisticsUiItem,
        val refreshing: Boolean = false,
        @StringRes val messageId: Int? = null
    ) : QrSettingsState()

    object Error : QrSettingsState()
}