package com.konovalovea.sbpadmin.api.repository

import com.konovalovea.sbpadmin.api.QrApiService
import com.konovalovea.sbpadmin.api.entity.Operation
import com.konovalovea.sbpadmin.api.entity.Order
import com.konovalovea.sbpadmin.api.entity.QrApi
import com.konovalovea.sbpadmin.api.entity.Statistics
import javax.inject.Inject

interface QrRepository {

    suspend fun createOrder(order: Order): Order

    suspend fun getOrder(qrId: String): Order

    suspend fun getStatistics(qrId: String): Statistics

    suspend fun clearStatistics(qrId: String)
}

class QrRepositoryImpl @Inject constructor(
    private val qrApiService: QrApiService
) : QrRepository {

    override suspend fun createOrder(order: Order): Order {
        return qrApiService.createOrder(order)
    }

    override suspend fun getOrder(qrId: String): Order {
        return qrApiService.getOrder(qrId)
    }

    override suspend fun getStatistics(qrId: String): Statistics {
        return qrApiService.getStats(qrId)
    }

    override suspend fun clearStatistics(qrId: String) {
        qrApiService.clearStats(qrId)
    }
}

class QrRepositoryTestImpl @Inject constructor() : QrRepository {

    override suspend fun createOrder(order: Order): Order {
        return Order(
            id = "1",
            amount = order.amount,
            qr = QrApi(
                id = "1",
                additionalInfo = "Дополнительная информация",
                paymentDetails = "Детали платежа"
            )
        )
    }

    override suspend fun getOrder(qrId: String): Order {
        return Order(
            id = "1",
            amount = 1467.0,
            qr = QrApi(
                id = "1",
                additionalInfo = "Дополнительная информация",
                paymentDetails = "Детали платежа"
            )
        )
    }

    override suspend fun getStatistics(qrId: String): Statistics {
        return Statistics(
            totalSum = 100.0,
            orderCount = 10,
            operations = listOf(
                Operation(
                    id = "id",
                    date = "2000-01-23T01:23:45.678+09:00",
                    amount = 10.0,
                    isDone = true
                ),
                Operation(
                    id = "id",
                    date = "2000-01-23T01:23:45.678+09:00",
                    amount = 10.0,
                    isDone = true
                ),
                Operation(
                    id = "id",
                    date = "2000-01-23T01:23:45.678+09:00",
                    amount = 10.0,
                    isDone = true
                )
            )
        )
    }

    override suspend fun clearStatistics(qrId: String) {

    }
}