package com.konovalovea.sbpadmin.api.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Order(
    @SerialName("id")
    val id: String? = null,
    @SerialName("amount")
    val amount: Double?,
    @SerialName("qr")
    val qr: QrApi? = null
)