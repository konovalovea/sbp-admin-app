package com.konovalovea.sbpadmin.api.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class FiscType() {
    @SerialName("NOFISC")
    NOFISC,
    @SerialName("FISC105")
    FISC105,
    @SerialName("FISC12")
    FISC12
}