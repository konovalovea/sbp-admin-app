package com.konovalovea.sbpadmin.api.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Statistics(
    @SerialName("totalSum")
    val totalSum: Double?,
    @SerialName("orderCount")
    val orderCount: Int?,
    @SerialName("orders")
    val operations: List<Operation>?
)