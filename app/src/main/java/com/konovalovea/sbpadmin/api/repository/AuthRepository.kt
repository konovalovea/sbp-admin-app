package com.konovalovea.sbpadmin.api.repository

import com.konovalovea.sbpadmin.api.QrApiService
import com.konovalovea.sbpadmin.api.SecretKeyRequest
import javax.inject.Inject

interface AuthRepository {

    suspend fun setApiKey(key: String): Boolean
}

class AuthRepositoryImpl @Inject constructor(
    private val qrApiService: QrApiService
): AuthRepository {

    override suspend fun setApiKey(key: String): Boolean {
        // TODO better exception handling
        return try {
            qrApiService.setSecretKey(QrApiService.LOGIN, SecretKeyRequest(key))
            true
        } catch (e: Exception) {
            false
        }
    }
}

class AuthRepositoryTestImpl @Inject constructor(): AuthRepository {

    override suspend fun setApiKey(key: String): Boolean {
        return true
    }
}
