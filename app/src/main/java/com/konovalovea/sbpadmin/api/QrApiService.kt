package com.konovalovea.sbpadmin.api

import com.konovalovea.sbpadmin.api.entity.FiscResponse
import com.konovalovea.sbpadmin.api.entity.Order
import com.konovalovea.sbpadmin.api.entity.Statistics
import retrofit2.http.*


interface QrApiService {

    @Headers("login: login")
    @POST("/createOrder")
    suspend fun createOrder(@Body order: Order): Order

    @Headers("login: login")
    @GET("/getOrder/{qrId}")
    suspend fun getOrder(@Path("qrId") qrId: String): Order

    @PUT("/{login}/setSecretKey")
    suspend fun setSecretKey(
        @Path("login") login: String,
        @Body secretKeyRequest: SecretKeyRequest
    )

    @Headers("login: login")
    // hardcoded login
    @GET("/login/getFiscType")
    suspend fun getFiscType(): FiscResponse

    @Headers("login: login")
    // hardcoded login
    @PUT("/login/setFiscType/{fiscType}")
    suspend fun setFiscType(
        @Path("fiscType") fiscType: String
    )

    @Headers("login: login")
    @GET("/getStats/{qrId}")
    suspend fun getStats(@Path("qrId") qrId: String): Statistics

    @Headers("login: login")
    @DELETE("/clearStats/{qrId}")
    suspend fun clearStats(@Path("qrId") qrId: String)

    companion object {
        const val LOGIN = "login"
    }
}