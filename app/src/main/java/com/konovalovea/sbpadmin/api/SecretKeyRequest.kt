package com.konovalovea.sbpadmin.api

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class SecretKeyRequest(
    @SerialName("secretKey")
    val secretKey: String
)