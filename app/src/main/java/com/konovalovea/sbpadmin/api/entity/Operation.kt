package com.konovalovea.sbpadmin.api.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class Operation(
    @SerialName("orderId")
    val id: String,
    @SerialName("buyDate")
    val date: String,
    @SerialName("amount")
    val amount: Double,
    @SerialName("isDone")
    val isDone: Boolean
)