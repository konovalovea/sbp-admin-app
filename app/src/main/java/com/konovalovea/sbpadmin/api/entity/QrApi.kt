package com.konovalovea.sbpadmin.api.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
class QrApi(
    @SerialName("id")
    val id: String,
    @SerialName("additionalInfo")
    val additionalInfo: String? = null,
    @SerialName("paymentDetails")
    val paymentDetails: String? = null
)