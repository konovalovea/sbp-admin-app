package com.konovalovea.sbpadmin.api.repository

import com.konovalovea.sbpadmin.api.QrApiService
import com.konovalovea.sbpadmin.api.entity.FiscType
import javax.inject.Inject

interface PaySettingsRepository {

    // probably there would be a get method
    suspend fun getFiscType(): FiscType

    suspend fun setFiscType(fiscType: FiscType)

    suspend fun enableFiscalization(
        version: String
    ): Boolean

    suspend fun disableFiscalization(): Boolean
}

class PaySettingsRepositoryImpl @Inject constructor(
    private val qrApiService: QrApiService
) : PaySettingsRepository {

    override suspend fun getFiscType(): FiscType {
        return qrApiService.getFiscType().fiscType
    }

    override suspend fun setFiscType(fiscType: FiscType) {
        qrApiService.setFiscType(fiscType.name)
    }

    override suspend fun enableFiscalization(version: String): Boolean {
        return true
    }

    override suspend fun disableFiscalization(): Boolean {
        return true
    }
}

class TestPaySettingsRepositoryImpl @Inject constructor() : PaySettingsRepository {

    override suspend fun getFiscType(): FiscType {
        return FiscType.FISC105
    }

    override suspend fun setFiscType(fiscType: FiscType) {
        return
    }

    override suspend fun enableFiscalization(version: String): Boolean {
        return true
    }

    override suspend fun disableFiscalization(): Boolean {
        return true
    }
}