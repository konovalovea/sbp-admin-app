package com.konovalovea.sbpadmin.api.entity

import kotlinx.serialization.Serializable

@Serializable
class FiscResponse(
    val fiscType: FiscType
)