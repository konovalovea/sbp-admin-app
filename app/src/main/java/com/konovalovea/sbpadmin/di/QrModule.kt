package com.konovalovea.sbpadmin.di

import com.konovalovea.sbpadmin.api.repository.*
import com.konovalovea.sbpadmin.ui.common.util.ClipboardHelper
import com.konovalovea.sbpadmin.ui.common.util.ClipboardHelperImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
abstract class QrModule {

    @Binds
    abstract fun bindQrRepository(qrRepositoryImpl: QrRepositoryImpl): QrRepository

    @Binds
    abstract fun bindClipBoardHelper(clipboardHelperImpl: ClipboardHelperImpl): ClipboardHelper

    @Binds
    abstract fun bindAuthRepository(authRepositoryImpl: AuthRepositoryImpl): AuthRepository

    @Binds
    abstract fun bindPaySettingsRepository(paySettingsRepositoryImpl: PaySettingsRepositoryImpl): PaySettingsRepository
}